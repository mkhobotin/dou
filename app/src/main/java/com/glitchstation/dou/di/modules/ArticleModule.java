package com.glitchstation.dou.di.modules;

import com.glitchstation.dou.modules.article.contract.FullArticleContract;
import com.glitchstation.dou.modules.article.presenter.FullArticlePresenter;
import com.glitchstation.dou.modules.article.view.FullArticleActivity;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ArticleModule {

    @Binds
    abstract FullArticleContract.View provideFullArticleView(FullArticleActivity activity);

    @Binds
    abstract FullArticleContract.Presenter provideFullArticlePresenter(FullArticlePresenter presenter);
}
