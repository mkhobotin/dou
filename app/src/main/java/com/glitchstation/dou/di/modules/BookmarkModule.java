package com.glitchstation.dou.di.modules;

import com.glitchstation.dou.modules.bookmark.contract.BookmarksTabContract;
import com.glitchstation.dou.modules.bookmark.presenter.BookmarksTabPresenter;
import com.glitchstation.dou.modules.bookmark.view.BookmarksTabFragment;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class BookmarkModule {

    @Binds
    abstract BookmarksTabContract.View provideBookmarksTabView(BookmarksTabFragment fragment);

    @Binds
    abstract BookmarksTabContract.Presenter provideBookmarksTabPresenter(BookmarksTabPresenter presenter);
}
