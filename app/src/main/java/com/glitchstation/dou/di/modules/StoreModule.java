package com.glitchstation.dou.di.modules;

import com.glitchstation.dou.modules.store.db.ArticleDBProvider;
import com.glitchstation.dou.modules.store.db.ArticleRealmProvider;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class StoreModule {

    @Binds
    abstract ArticleDBProvider provideDbProvider(ArticleRealmProvider provider);
}
