package com.glitchstation.dou.di.modules;

import com.glitchstation.dou.modules.feed.contract.FeedTabContract;
import com.glitchstation.dou.modules.feed.presenter.FeedTabPresenter;
import com.glitchstation.dou.modules.feed.view.FeedTabFragment;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class FeedModule {

    @Binds
    abstract FeedTabContract.View provideFeedTabView(FeedTabFragment fragment);

    @Binds
    abstract FeedTabContract.Presenter provideFeedTabPresenter(FeedTabPresenter presenter);
}
