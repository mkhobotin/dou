package com.glitchstation.dou.di.modules;

import android.content.Context;

import com.glitchstation.dou.di.annotations.ActivityContext;
import com.glitchstation.dou.modules.category.contract.CategoriesTabContract;
import com.glitchstation.dou.modules.category.contract.CategoryContract;
import com.glitchstation.dou.modules.category.presenter.CategoriesTabPresenter;
import com.glitchstation.dou.modules.category.presenter.CategoryPresenter;
import com.glitchstation.dou.modules.category.view.CategoriesTabFragment;
import com.glitchstation.dou.modules.category.view.CategoryActivity;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class CategoryModule {

    @Binds
    abstract CategoriesTabContract.View provideCategoriesTabView(CategoriesTabFragment fragment);

    @Binds
    abstract CategoriesTabContract.Presenter provideCategoriesTabPresenter(CategoriesTabPresenter presenter);

    @Binds
    abstract CategoryContract.View provideCategoryView(CategoryActivity activity);

    @Binds
    abstract CategoryContract.Presenter provideCategoryPresenter(CategoryPresenter presenter);

    @Binds
    @ActivityContext
    abstract Context provideContext(CategoryActivity activity);
}
