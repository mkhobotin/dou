package com.glitchstation.dou.di.modules.androidinjector;

import com.glitchstation.dou.di.annotations.ActivityScope;
import com.glitchstation.dou.di.modules.ArticleModule;
import com.glitchstation.dou.di.modules.CategoryModule;
import com.glitchstation.dou.di.modules.MainModule;
import com.glitchstation.dou.modules.article.view.FullArticleActivity;
import com.glitchstation.dou.modules.category.view.CategoryActivity;
import com.glitchstation.dou.modules.main.view.MainActivity;
import com.glitchstation.dou.modules.splash.view.SplashActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector
    abstract SplashActivity injectSplashActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = {MainModule.class, MainFragmentsBindingModule.class})
    abstract MainActivity injectMainActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = ArticleModule.class)
    abstract FullArticleActivity injectFullArticleActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = CategoryModule.class)
    abstract CategoryActivity injectCategoryActivity();
}
