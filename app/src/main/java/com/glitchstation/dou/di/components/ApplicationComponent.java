package com.glitchstation.dou.di.components;

import com.glitchstation.dou.di.modules.ApplicationModule;
import com.glitchstation.dou.di.modules.androidinjector.ActivityBindingModule;
import com.glitchstation.dou.modules.statics.MyApplication;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {ApplicationModule.class,
    AndroidInjectionModule.class, ActivityBindingModule.class})
public interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(MyApplication application);
        ApplicationComponent build();
    }

    void

    inject(MyApplication application);
}
