package com.glitchstation.dou.di.modules;

import com.glitchstation.dou.modules.article.service.ArticleClient;
import com.glitchstation.dou.modules.article.service.ArticleHttpService;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class HttpServiceModule {

    @Provides
    static ArticleClient provideArticleClient(ArticleHttpService service) {
        return service.getClient();
    }
}
