package com.glitchstation.dou.di.modules;

import android.content.Context;

import com.glitchstation.dou.modules.statics.MyApplication;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;

@Module(includes = {HttpServiceModule.class, StoreModule.class})
public class ApplicationModule {

    @Provides
    static Context provideContext(MyApplication application) {
        return application.getApplicationContext();
    }

    @Provides
    static Realm provideRealm() {
        return Realm.getDefaultInstance();
    }
}
