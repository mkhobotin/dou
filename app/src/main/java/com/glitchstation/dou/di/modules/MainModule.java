package com.glitchstation.dou.di.modules;

import android.support.v4.app.FragmentManager;

import com.glitchstation.dou.modules.main.contract.MainContract;
import com.glitchstation.dou.modules.main.presenter.MainPresenter;
import com.glitchstation.dou.modules.main.view.MainActivity;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class MainModule {

    @Binds
    abstract MainContract.View provideMainView(MainActivity activity);

    @Binds
    abstract MainContract.Presenter provideMainPresenter(MainPresenter presenter);

    @Provides
    static FragmentManager provideFragmentManager(MainActivity activity) {
        return activity.getSupportFragmentManager();
    }
}