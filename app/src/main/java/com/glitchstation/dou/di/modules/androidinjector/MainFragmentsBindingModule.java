package com.glitchstation.dou.di.modules.androidinjector;

import android.content.Context;

import com.glitchstation.dou.di.annotations.ActivityContext;
import com.glitchstation.dou.di.annotations.FragmentScope;
import com.glitchstation.dou.di.modules.BookmarkModule;
import com.glitchstation.dou.di.modules.CategoryModule;
import com.glitchstation.dou.di.modules.FeedModule;
import com.glitchstation.dou.modules.bookmark.view.BookmarksTabFragment;
import com.glitchstation.dou.modules.category.view.CategoriesTabFragment;
import com.glitchstation.dou.modules.feed.view.FeedTabFragment;
import com.glitchstation.dou.modules.main.view.MainActivity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainFragmentsBindingModule {

    @FragmentScope
    @ContributesAndroidInjector(modules = FeedModule.class)
    abstract FeedTabFragment injectFeedTabFragment();

    @FragmentScope
    @ContributesAndroidInjector(modules = CategoryModule.class)
    abstract CategoriesTabFragment injectCategoriesTabFragment();

    @FragmentScope
    @ContributesAndroidInjector(modules = BookmarkModule.class)
    abstract BookmarksTabFragment injectBookmarksTabFragment();

    @Binds
    @ActivityContext
    abstract Context provideContext(MainActivity activity);
}
