package com.glitchstation.dou.modules.category.view;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.glitchstation.dou.R;
import com.glitchstation.dou.di.annotations.FragmentScope;
import com.glitchstation.dou.modules.analytics.EventsTracker;
import com.glitchstation.dou.modules.article.model.ArticlePreview;
import com.glitchstation.dou.modules.article.view.ArticlePreviewListAdapter;
import com.glitchstation.dou.modules.category.contract.CategoriesTabContract;
import com.glitchstation.dou.modules.category.presenter.CategoriesTabPresenter;
import com.glitchstation.dou.modules.common.Notifier;

import java.util.ArrayList;
import java.util.Arrays;

import javax.inject.Inject;

import butterknife.BindArray;
import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;
import pl.droidsonroids.gif.GifImageView;

@FragmentScope
public class CategoriesTabFragment extends Fragment
    implements CategoriesTabContract.View {
    @Inject
    CategoriesTabPresenter presenter;
    @Inject
    Notifier notifier;
    @Inject
    EventsTracker eventsTracker;
    @BindView(R.id.categories_tab_list_container)
    LinearLayout categoryListContainer;
    @BindView(R.id.categories_tab_progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.categories_tab_error_img)
    GifImageView errorImage;
    @BindView(R.id.categories_tab_swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindArray(R.array.categories_values)
    String[] categoriesValues;
    @BindArray(R.array.categories_titles)
    String[] categoriesTitles;
    private ArrayList<String> categories;
    private ArrayList<ArticlePreviewListAdapter> adapters;
    private boolean allCategoriesLoaded = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_categories_tab, container, false);
        ButterKnife.bind(this, view);
        setUi();
        presenter.getArticlesOfCategories(categoriesValues);
        return view;
    }

    @Override
    public void onDestroyView() {
        presenter.unsubscribe();
        super.onDestroyView();
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void setCategoryArticles(String category, ArrayList<ArticlePreview> articles) {
        adapters.get(categories.indexOf(category)).setArticles(articles);
    }

    @Override
    public void showCategories() {
        swipeRefreshLayout.setRefreshing(false);
        progressBar.setVisibility(View.GONE);
        errorImage.setVisibility(View.GONE);
        categoryListContainer.setVisibility(View.VISIBLE);
        allCategoriesLoaded = true;
    }

    @Override
    public void onCategoriesLoadError() {
        swipeRefreshLayout.setRefreshing(false);
        progressBar.setVisibility(View.GONE);
        if (!allCategoriesLoaded) {
            errorImage.setVisibility(View.VISIBLE);
        } else {
            notifier.showMessage(getActivity(), R.string.no_connection_error);
        }
    }

    private void setUi() {
        categories = new ArrayList<>();
        adapters = new ArrayList<>();
        categories.addAll(Arrays.asList(categoriesValues));

        int width = Resources.getSystem().getDisplayMetrics().widthPixels;
        int paddingEnd = Math.round((1.0f - ArticlePreviewListAdapter.narrowedSizeMultiplier) * width);
        int shift = Math.round((1.0f - ArticlePreviewListAdapter.narrowedSizeMultiplier) / 2.0f * width);

        for (int i = 0; i < categories.size(); i++) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.item_category, categoryListContainer, false);

            if (i == categories.size() - 1) {
                view.findViewById(R.id.item_category_divider).setVisibility(View.GONE);
            }

            TextView title = view.findViewById(R.id.item_category_title);
            title.setText(categoriesTitles[i]);

            RecyclerView articleList = view.findViewById(R.id.item_category_article_list);
            articleList.setLayoutManager(new LinearLayoutManager(
                getActivity(), LinearLayoutManager.HORIZONTAL, false));

            articleList.setClipToPadding(false);
            articleList.setPadding(0, 0, paddingEnd, 0);

            ArticlePreviewListAdapter adapter = new ArticlePreviewListAdapter(
                getActivity(), ArticlePreviewListAdapter.ListType.HORIZONTAL, eventsTracker);
            articleList.setAdapter(adapter);
            adapters.add(adapter);

            SnapHelper snapHelper = new PagerSnapHelper() {
                @Nullable
                @Override
                public int[] calculateDistanceToFinalSnap(@NonNull RecyclerView.LayoutManager layoutManager, @NonNull View targetView) {
                    int[] out =  super.calculateDistanceToFinalSnap(layoutManager, targetView);
                    if (out != null) {
                        out[0] += shift;
                    }
                    return out;
                }
            };
            snapHelper.attachToRecyclerView(articleList);

            final int categoryPosition = i;
            view.findViewById(R.id.item_category_title_container).setOnClickListener(v -> {
                eventsTracker.logCustomEvent("See all button click",
                    "category", categoriesTitles[categoryPosition]);
                Intent intent = new Intent(getActivity(), CategoryActivity.class);
                intent.putExtra("categoryPosition", categoryPosition);
                startActivity(intent);
            });

            categoryListContainer.addView(view);
        }

        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.getArticlesOfCategories(categoriesValues));
    }
}
