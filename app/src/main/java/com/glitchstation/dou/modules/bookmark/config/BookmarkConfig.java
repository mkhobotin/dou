package com.glitchstation.dou.modules.bookmark.config;

import com.glitchstation.dou.modules.article.model.FullArticle;
import com.glitchstation.dou.modules.eventbus.EventConfig;
import com.glitchstation.dou.modules.eventbus.EventType;

public class BookmarkConfig implements EventConfig {
    private FullArticle article;
    private EventType eventType;

    public BookmarkConfig(FullArticle article, EventType eventType) {
        this.article = article;
        this.eventType = eventType;
    }

    public FullArticle getArticle() {
        return article;
    }

    public EventType getEventType() {
        return eventType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BookmarkConfig that = (BookmarkConfig) o;

        if (article != null ? !article.equals(that.article) : that.article != null) return false;
        return eventType == that.eventType;
    }

    @Override
    public int hashCode() {
        int result = article != null ? article.hashCode() : 0;
        result = 31 * result + (eventType != null ? eventType.hashCode() : 0);
        return result;
    }
}
