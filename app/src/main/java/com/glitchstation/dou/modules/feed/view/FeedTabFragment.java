package com.glitchstation.dou.modules.feed.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.glitchstation.dou.R;
import com.glitchstation.dou.di.annotations.FragmentScope;
import com.glitchstation.dou.modules.article.model.ArticlePreview;
import com.glitchstation.dou.modules.article.view.ArticlePreviewListAdapter;
import com.glitchstation.dou.modules.common.Notifier;
import com.glitchstation.dou.modules.feed.contract.FeedTabContract;
import com.glitchstation.dou.modules.utils.pagination.Paginate;
import com.glitchstation.dou.modules.utils.pagination.RecyclerPaginate;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;
import pl.droidsonroids.gif.GifImageView;

@FragmentScope
public class FeedTabFragment extends Fragment
    implements FeedTabContract.View {
    @Inject
    FeedTabContract.Presenter presenter;
    @Inject
    ArticlePreviewListAdapter adapter;
    @Inject
    Notifier notifier;
    @BindView(R.id.feed_tab_recycler_view)
    RecyclerView feedRecyclerView;
    @BindView(R.id.feed_tab_error_img)
    GifImageView errorImage;
    @BindView(R.id.feed_tab_swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.feed_tab_progress_bar)
    ProgressBar progressBar;
    private Paginate paginate;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed_tab, container, false);
        ButterKnife.bind(this, view);
        setUi();
        return view;
    }

    @Override
    public void onDestroyView() {
        presenter.unsubscribe();
        super.onDestroyView();
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    private void setUi() {
        feedRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        feedRecyclerView.setAdapter(adapter);
        paginate = RecyclerPaginate
            .with(feedRecyclerView, presenter)
            .setLoadingTriggerThreshold(10)
            .build();
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.getFeedArticles());
    }

    @Override
    public void showArticles(ArrayList<ArticlePreview> articles, boolean refresh) {
        swipeRefreshLayout.setRefreshing(false);
        progressBar.setVisibility(View.GONE);
        errorImage.setVisibility(View.GONE);
        feedRecyclerView.setVisibility(View.VISIBLE);
        if (refresh) {
            adapter.setArticles(articles);
        } else {
            adapter.addArticles(articles);
        }
    }

    @Override
    public void onArticlesLoadError() {
        progressBar.setVisibility(View.GONE);
        if (adapter.getItemCount() != 0) {
            if (swipeRefreshLayout.isRefreshing()) {
                notifier.showMessage(getActivity(), R.string.no_connection_error);
            } else {
                paginate.showErrorRow();
            }
        } else {
            errorImage.setVisibility(View.VISIBLE);
        }
        swipeRefreshLayout.setRefreshing(false);
    }

}
