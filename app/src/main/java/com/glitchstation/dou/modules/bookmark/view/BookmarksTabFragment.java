package com.glitchstation.dou.modules.bookmark.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.glitchstation.dou.R;
import com.glitchstation.dou.di.annotations.FragmentScope;
import com.glitchstation.dou.modules.article.model.ArticlePreview;
import com.glitchstation.dou.modules.article.model.FullArticle;
import com.glitchstation.dou.modules.article.view.ArticlePreviewListAdapter;
import com.glitchstation.dou.modules.bookmark.contract.BookmarksTabContract;
import com.glitchstation.dou.modules.common.Notifier;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;

@FragmentScope
public class BookmarksTabFragment extends Fragment
    implements BookmarksTabContract.View {
    @Inject
    BookmarksTabContract.Presenter presenter;
    @Inject
    ArticlePreviewListAdapter adapter;
    @Inject
    Notifier notifier;
    @BindView(R.id.bookmarks_tab_recycler_view)
    RecyclerView bookmarkRecyclerView;
    @BindView(R.id.bookmarks_tab_progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.bookmarks_tab_swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.bookmarks_tab_no_bookmarks_layout)
    LinearLayout noBookmarksLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookmarks_tab, container, false);
        ButterKnife.bind(this, view);
        setUi();
        presenter.getBookmarks();
        return view;
    }

    @Override
    public void onDestroyView() {
        presenter.unsubscribe();
        super.onDestroyView();
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    private void setUi() {
        bookmarkRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        bookmarkRecyclerView.setAdapter(adapter);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.getBookmarks());
    }

    @Override
    public void showBookmarks(ArrayList<FullArticle> articles) {
        swipeRefreshLayout.setRefreshing(false);
        progressBar.setVisibility(View.GONE);
        bookmarkRecyclerView.setVisibility(View.VISIBLE);
        ArrayList<ArticlePreview> arrayList = new ArrayList<>();
        for (FullArticle article : articles) {
            arrayList.add(article.getPreview());
        }
        if (arrayList.isEmpty()) {
            noBookmarksLayout.setVisibility(View.VISIBLE);
        } else {
            noBookmarksLayout.setVisibility(View.GONE);
        }
        adapter.setArticles(arrayList);
    }

    @Override
    public void onBookmarksLoadError() {
        swipeRefreshLayout.setRefreshing(false);
        progressBar.setVisibility(View.GONE);
        notifier.showMessage(getActivity(), R.string.unknown_error);
    }
}
