package com.glitchstation.dou.modules.utils.pagination;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.glitchstation.dou.R;

class WrapperAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    static final int ITEM_VIEW_TYPE_LOADING = Integer.MAX_VALUE - 50;
    static final int ITEM_VIEW_TYPE_ERROR = Integer.MAX_VALUE - 49;
    private final RecyclerView.Adapter wrappedAdapter;
    private final Paginate.Callbacks callbacks;
    private boolean displayLoadingRow = true;
    private boolean displayErrorRow = false;

    public WrapperAdapter(RecyclerView.Adapter adapter, Paginate.Callbacks callbacks) {
        this.wrappedAdapter = adapter;
        this.callbacks = callbacks;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ITEM_VIEW_TYPE_ERROR) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagination_error_row, parent, false);
            return new RecyclerView.ViewHolder(view){};
        } else if (viewType == ITEM_VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pagination_loading_row, parent, false);
            return new RecyclerView.ViewHolder(view){};
        } else {
            return wrappedAdapter.onCreateViewHolder(parent, viewType);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (isErrorRow(position)) {
            holder.itemView.setOnClickListener(v -> {
                setDisplayErrorRow(false);
                callbacks.onLoadMore();
            });
        } else if (!isLoadingRow(position)) {
            wrappedAdapter.onBindViewHolder(holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return displayLoadingRow ? wrappedAdapter.getItemCount() + 1 : wrappedAdapter.getItemCount();
    }

    @Override
    public int getItemViewType(int position) {
        if (isErrorRow(position)) {
            return ITEM_VIEW_TYPE_ERROR;
        } else if (isLoadingRow(position)) {
            return ITEM_VIEW_TYPE_LOADING;
        } else {
            return wrappedAdapter.getItemViewType(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return isLoadingRow(position) ? RecyclerView.NO_ID : wrappedAdapter.getItemId(position);
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
        wrappedAdapter.setHasStableIds(hasStableIds);
    }

    public RecyclerView.Adapter getWrappedAdapter() {
        return wrappedAdapter;
    }

    boolean isDisplayLoadingRow() {
        return displayLoadingRow;
    }

    boolean isDisplayErrorRow() {
        return displayErrorRow;
    }

    void setDisplayLoadingRow(boolean displayLoadingRow) {
        if (this.displayLoadingRow != displayLoadingRow) {
            this.displayLoadingRow = displayLoadingRow;
            notifyDataSetChanged();
        }
    }

    void setDisplayErrorRow(boolean displayErrorRow) {
        if (this.displayErrorRow != displayErrorRow) {
            this.displayErrorRow = displayErrorRow;
            notifyDataSetChanged();
        }
    }

    boolean isLoadingRow(int position) {
        return displayLoadingRow && position == getLoadingRowPosition();
    }

    boolean isErrorRow(int position) {
        return displayErrorRow && position == getLoadingRowPosition();
    }

    private int getLoadingRowPosition() {
        return displayLoadingRow ? getItemCount() - 1 : -1;
    }
}