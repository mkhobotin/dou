package com.glitchstation.dou.modules.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class DateFormatter {
    private SimpleDateFormat sourceDateFormat;
    private SimpleDateFormat outputDateFormat;

    public DateFormatter() {
        sourceDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", new Locale("ru","RU"));
        outputDateFormat = new SimpleDateFormat("dd MMMM, hh:mm", new Locale("ru","RU"));
    }

    public String formatDate(String source) throws ParseException {
        return outputDateFormat.format(sourceDateFormat.parse(source));
    }
}
