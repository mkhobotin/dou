package com.glitchstation.dou.modules.common;

import com.glitchstation.dou.modules.article.model.ArticlePreview;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class GsonFactory {
    private Gson gson;

    @Inject
    public GsonFactory() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(
            new TypeToken<ArrayList<ArticlePreview>>(){}.getType(), getArticlePreviewDeserializer());
        gson = gsonBuilder.create();
    }

    public Gson getGson() {
        return gson;
    }

    private JsonDeserializer<ArrayList<ArticlePreview>> getArticlePreviewDeserializer() {
        return (json, typeOfT, context) -> {
            ArrayList<ArticlePreview> articles = new ArrayList<>();
            for (JsonElement article : json.getAsJsonObject().get("results").getAsJsonArray()) {
                articles.add(gson.fromJson(article.getAsJsonObject(), ArticlePreview.class));
            }
            return articles;
        };
    }
}
