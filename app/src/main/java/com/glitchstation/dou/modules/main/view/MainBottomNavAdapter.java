package com.glitchstation.dou.modules.main.view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.glitchstation.dou.modules.bookmark.view.BookmarksTabFragment;
import com.glitchstation.dou.modules.category.view.CategoriesTabFragment;
import com.glitchstation.dou.modules.feed.view.FeedTabFragment;

import java.util.ArrayList;

import javax.inject.Inject;

public class MainBottomNavAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> fragments;

    @Inject
    public MainBottomNavAdapter(FragmentManager fm) {
        super(fm);
        fragments = new ArrayList<>();
        fragments.add(new FeedTabFragment());
        fragments.add(new CategoriesTabFragment());
        fragments.add(new BookmarksTabFragment());
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
