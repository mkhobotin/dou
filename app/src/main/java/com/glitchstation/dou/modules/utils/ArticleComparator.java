package com.glitchstation.dou.modules.utils;

import com.glitchstation.dou.modules.article.model.FullArticle;

import java.util.Comparator;

public class ArticleComparator implements Comparator<FullArticle> {

    @Override
    public int compare(FullArticle o1, FullArticle o2) {
        return -o1.getPreview().getPublished().compareTo(o2.getPreview().getPublished());
    }
}
