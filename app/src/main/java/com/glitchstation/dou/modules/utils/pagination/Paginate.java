package com.glitchstation.dou.modules.utils.pagination;

import android.support.v7.widget.RecyclerView;

public abstract class Paginate {

    public interface Callbacks {

        void onLoadMore();

        boolean isLoading();

        boolean hasLoadedAllItems();
    }

    abstract public void setDisplayLoadingRow(boolean displayLoadingRow);

    abstract public void unbind();

    abstract public void showErrorRow();

    public static RecyclerPaginate.Builder with(RecyclerView recyclerView, Callbacks callback) {
        return new RecyclerPaginate.Builder(recyclerView, callback);
    }
}