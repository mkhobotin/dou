package com.glitchstation.dou.modules.article.config;

import com.glitchstation.dou.modules.eventbus.EventConfig;

public class ArticlePreviewConfig implements EventConfig {
    private String category;
    private Integer offset;
    private Integer limit;
    private boolean cached;

    public ArticlePreviewConfig(String category, Integer offset, Integer limit, boolean cached) {
        this.category = category;
        this.offset = offset;
        this.limit = limit;
        this.cached = cached;
    }

    public String getCategory() {
        return category;
    }

    public Integer getOffset() {
        return offset;
    }

    public Integer getLimit() {
        return limit;
    }

    public boolean isCached() {
        return cached;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArticlePreviewConfig that = (ArticlePreviewConfig) o;

        if (category != null ? !category.equals(that.category) : that.category != null)
            return false;
        if (offset != null ? !offset.equals(that.offset) : that.offset != null) return false;
        return limit != null ? limit.equals(that.limit) : that.limit == null;
    }

    @Override
    public int hashCode() {
        int result = category != null ? category.hashCode() : 0;
        result = 31 * result + (offset != null ? offset.hashCode() : 0);
        result = 31 * result + (limit != null ? limit.hashCode() : 0);
        return result;
    }
}
