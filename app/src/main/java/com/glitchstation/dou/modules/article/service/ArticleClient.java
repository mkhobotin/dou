package com.glitchstation.dou.modules.article.service;

import com.glitchstation.dou.modules.article.model.ArticleHtml;
import com.glitchstation.dou.modules.article.model.ArticlePreview;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ArticleClient {

    @GET("articles/")
    Observable<Response<ArrayList<ArticlePreview>>> getFeedArticles(
        @Query("offset") int offset);

    @GET("articles/")
    Observable<Response<ArrayList<ArticlePreview>>> getCategoryArticles(
        @Query("category") String category,
        @Query("limit") int limit,
        @Query("offset") int offset);

    @GET
    Observable<Response<ArticleHtml>> getArticleHtml(
        @Url String url);
}
