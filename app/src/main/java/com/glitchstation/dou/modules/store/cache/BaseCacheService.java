package com.glitchstation.dou.modules.store.cache;

import com.glitchstation.dou.modules.eventbus.EventConfig;

import java.util.HashMap;

import javax.inject.Inject;

public class BaseCacheService implements CacheService {
    private HashMap<EventConfig, Object> cache;

    @Inject
    public BaseCacheService() {
        cache = new HashMap<>();
    }

    @Override
    public Object getData(EventConfig config) {
        return cache.get(config);
    }

    @Override
    public void saveData(EventConfig config, Object data) {
        cache.put(config, data);
    }

    @Override
    public void deleteData(EventConfig config) {
        cache.remove(config);
    }
}
