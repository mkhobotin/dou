package com.glitchstation.dou.modules.store.cache;

import com.glitchstation.dou.modules.eventbus.EventConfig;

public interface CacheService {

    Object getData(EventConfig config);

    void saveData(EventConfig config, Object data);

    void deleteData(EventConfig config);

}