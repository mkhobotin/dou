package com.glitchstation.dou.modules.article.presenter;

import com.glitchstation.dou.di.annotations.ActivityScope;
import com.glitchstation.dou.modules.article.config.FullArticleConfig;
import com.glitchstation.dou.modules.article.contract.FullArticleContract;
import com.glitchstation.dou.modules.article.model.ArticlePreview;
import com.glitchstation.dou.modules.article.model.FullArticle;
import com.glitchstation.dou.modules.bookmark.config.BookmarkConfig;
import com.glitchstation.dou.modules.eventbus.BaseEventService;
import com.glitchstation.dou.modules.eventbus.EventBus;

import javax.inject.Inject;

import static com.glitchstation.dou.modules.eventbus.EventType.DATA_DELETED;
import static com.glitchstation.dou.modules.eventbus.EventType.DATA_SAVED;
import static com.glitchstation.dou.modules.eventbus.EventType.DELETE_DATA;
import static com.glitchstation.dou.modules.eventbus.EventType.GET_DATA;
import static com.glitchstation.dou.modules.eventbus.EventType.SAVE_DATA;

@ActivityScope
public class FullArticlePresenter extends FullArticleContract.Presenter {
    private FullArticleContract.View view;
    private EventBus eventBus;

    @Inject
    public FullArticlePresenter(FullArticleContract.View view, EventBus eventBus) {
        this.view = view;
        this.eventBus = eventBus;
    }

    @Override
    public void getFullArticle(ArticlePreview preview, boolean cached) {
        FullArticleConfig config = new FullArticleConfig(preview, cached);
        addSubscription(config, new SubscriptionListener<FullArticle>() {
            @Override
            public void onSuccess(FullArticle article) {
                view.onArticleLoaded(article);
            }

            @Override
            public void onError(Exception e) {
                view.onArticleLoadError();
            }
        });
    }

    @Override
    public void addArticleToBookmarks(FullArticle article) {
        BookmarkConfig config = new BookmarkConfig(article, SAVE_DATA);
        addSubscription(config, eventBus.bookmark(), SAVE_DATA, DATA_SAVED, new SubscriptionListener<Void>() {
            @Override
            public void onSuccess(Void v) {
                view.onArticleBookmarked();
                eventBus.bookmark().emit(GET_DATA, new BookmarkConfig(null, GET_DATA));
            }

            @Override
            public void onError(Exception e) {
                view.onAddBookmarkError();
            }
        });
    }

    @Override
    public void removeArticleFromBookmarks(FullArticle article) {
        BookmarkConfig config = new BookmarkConfig(article, DELETE_DATA);
        addSubscription(config, eventBus.bookmark(), DELETE_DATA, DATA_DELETED, new SubscriptionListener<Void>() {
            @Override
            public void onSuccess(Void v) {
                view.onArticleUnbookmarked();
                eventBus.bookmark().emit(GET_DATA, new BookmarkConfig(null, GET_DATA));
            }

            @Override
            public void onError(Exception e) {
                view.onRemoveBookmarkError();
            }
        });
    }

    @Override
    public BaseEventService getEventService() {
        return eventBus.article();
    }
}
