package com.glitchstation.dou.modules.article.view;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.glitchstation.dou.BuildConfig;
import com.glitchstation.dou.R;
import com.glitchstation.dou.di.annotations.ActivityScope;
import com.glitchstation.dou.modules.analytics.EventsTracker;
import com.glitchstation.dou.modules.article.contract.FullArticleContract;
import com.glitchstation.dou.modules.article.model.ArticlePreview;
import com.glitchstation.dou.modules.article.model.FullArticle;
import com.glitchstation.dou.modules.common.Notifier;

import java.io.IOException;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

@ActivityScope
public class FullArticleActivity extends AppCompatActivity
    implements FullArticleContract.View, CustomSwipeRefreshLayout.CanChildScrollUpCallback {
    @Inject
    FullArticleContract.Presenter presenter;
    @Inject
    Notifier notifier;
    @Inject
    EventsTracker eventsTracker;
    @BindView(R.id.article_content)
    ArticleWebView contentWebView;
    @BindView(R.id.article_progress)
    ProgressBar progressBar;
    @BindView(R.id.article_toolbar)
    Toolbar toolbar;
    @BindView(R.id.article_error_img)
    GifImageView errorImage;
    @BindView(R.id.article_swipe_refresh)
    CustomSwipeRefreshLayout swipeRefreshLayout;
    private boolean isBookmarked;
    private GifImageView bookmarkImageBtn;
    private ArticlePreview articlePreview;
    private FullArticle fullArticle;
    private ShareSelectorReceiver shareReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);
        ButterKnife.bind(this);
        setUi();
        if (articlePreview != null) {
            presenter.getFullArticle(articlePreview, true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        contentWebView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        contentWebView.onPause();
    }

    @Override
    protected void onStop() {
        if (shareReceiver != null) {
            try {
                unregisterReceiver(shareReceiver);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        presenter.unsubscribe();
        super.onDestroy();
    }

    private void setUi() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            articlePreview = (ArticlePreview) bundle.getSerializable("articlePreview");
        }

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setElevation(4 * getResources().getDisplayMetrics().density);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setTitle("");
        }

        contentWebView.setGestureDetector(new GestureDetector(
            this, new WebViewGestureDetector(toolbar)));

        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        swipeRefreshLayout.setCanChildScrollUpCallback(this);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            if (articlePreview != null) {
                presenter.getFullArticle(articlePreview, false);
            }
        });
        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean canSwipeRefreshChildScrollUp() {
        return contentWebView.getScrollY() > 0;
    }

    @Override
    public void onArticleLoaded(FullArticle article) {
        swipeRefreshLayout.setRefreshing(false);
        progressBar.setVisibility(View.GONE);
        errorImage.setVisibility(View.GONE);
        contentWebView.setVisibility(View.VISIBLE);

        fullArticle = article;

        contentWebView.getSettings().setJavaScriptEnabled(true);
        contentWebView.loadDataWithBaseURL("file:///android_asset/",
            article.getHtml().getHtmlContent(), "text/html", "UTF-8", null);

        TextView title = toolbar.findViewById(R.id.toolbar_article_title);
        title.setText(article.getPreview().getAuthorName());

        TextView subtitle = toolbar.findViewById(R.id.toolbar_article_subtitle);
        String readTime;
        if (article.getHtml().getReadTime() == 0) {
            readTime = "< 1";
        } else {
            readTime = String.valueOf(article.getHtml().getReadTime());
        }
        subtitle.setText(getString(R.string.format_read_time, readTime));

        ImageView authorImage = toolbar.findViewById(R.id.toolbar_article_image);
        Glide
            .with(this)
            .load(article.getHtml().getAuthorImageUrl())
            .apply(RequestOptions.circleCropTransform())
            .into(authorImage);

        isBookmarked = article.isBookmarked();
        initBookmarkButton();
        initShareButton();
    }

    private void initBookmarkButton() {
        bookmarkImageBtn = toolbar.findViewById(R.id.toolbar_article_bookmark_btn);
        if (isBookmarked) {
            bookmarkImageBtn.setImageResource(R.drawable.bookmarked);
        } else {
            bookmarkImageBtn.setImageResource(R.drawable.unbookmarked);
        }
        bookmarkImageBtn.setOnClickListener(v -> {
            bookmarkImageBtn.setEnabled(false);
            if (!isBookmarked) {
                eventsTracker.logCustomEvent("Bookmark article", "action", "add");
                presenter.addArticleToBookmarks(fullArticle);
            } else {
                eventsTracker.logCustomEvent("Bookmark article", "action", "remove");
                presenter.removeArticleFromBookmarks(fullArticle);
            }
        });
    }

    private void initShareButton() {
        ImageView shareBtn = toolbar.findViewById(R.id.toolbar_article_share_btn);
        shareBtn.setOnClickListener(v -> {
            eventsTracker.logCustomEvent("Share button click");
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(Intent.EXTRA_TEXT,
                getString(R.string.format_share_message, articlePreview.getUrl(), BuildConfig.APPLICATION_ID));
            Intent chooser;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                if (shareReceiver == null) {
                    IntentFilter intentFilter = new IntentFilter(ShareSelectorReceiver.ACTION);
                    shareReceiver = new ShareSelectorReceiver();
                    registerReceiver(shareReceiver, intentFilter);
                }
                Intent receiver = new Intent(ShareSelectorReceiver.ACTION);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0,
                    receiver, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_ONE_SHOT);
                chooser = Intent.createChooser(sharingIntent, getResources().getString(R.string.share_title),
                    pendingIntent.getIntentSender());
            } else {
                chooser = Intent.createChooser(sharingIntent, getResources().getString(R.string.share_title));
            }
            startActivity(chooser);
        });
    }

    @Override
    public void onArticleLoadError() {
        swipeRefreshLayout.setRefreshing(false);
        progressBar.setVisibility(View.GONE);
        if (fullArticle == null) {
            errorImage.setVisibility(View.VISIBLE);
        } else {
            notifier.showMessage(this, R.string.no_connection_error);
        }
    }

    @Override
    public void onArticleBookmarked() {
        isBookmarked = true;
        bookmarkImageBtn.setEnabled(true);
        try {
            GifDrawable gif = new GifDrawable(getResources(), R.drawable.bookmark);
            gif.setLoopCount(1);
            gif.addAnimationListener(loopNumber -> bookmarkImageBtn.setImageResource(R.drawable.bookmarked));
            bookmarkImageBtn.setImageDrawable(gif);
        } catch (IOException e) {
            bookmarkImageBtn.setImageResource(R.drawable.bookmarked);
        }
        notifier.showMessage(this, R.string.article_bookmarked);
    }

    @Override
    public void onArticleUnbookmarked() {
        isBookmarked = false;
        bookmarkImageBtn.setEnabled(true);
        bookmarkImageBtn.setImageResource(R.drawable.unbookmarked);
        notifier.showMessage(this, R.string.article_unbookmarked);
    }

    @Override
    public void onAddBookmarkError() {
        bookmarkImageBtn.setEnabled(true);
        notifier.showMessage(this, R.string.add_bookmark_error);
    }

    @Override
    public void onRemoveBookmarkError() {
        bookmarkImageBtn.setEnabled(true);
        notifier.showMessage(this, R.string.remove_bookmark_error);
    }

    private class ShareSelectorReceiver extends BroadcastReceiver {
        private final static String ACTION = "DOU_READER_SHARE_ACTION";
        @Override
        public void onReceive(Context context, Intent intent) {
            for (String key : Objects.requireNonNull(intent.getExtras()).keySet()) {
                try {
                    ComponentName componentInfo = (ComponentName) intent.getExtras().get(key);
                    PackageManager packageManager = context.getPackageManager();
                    assert componentInfo != null;
                    String appName = (String) packageManager.getApplicationLabel(packageManager
                        .getApplicationInfo(componentInfo.getPackageName(), PackageManager.GET_META_DATA));
                    eventsTracker.logShare(appName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
