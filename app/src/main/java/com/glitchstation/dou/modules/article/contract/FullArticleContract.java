package com.glitchstation.dou.modules.article.contract;

import com.glitchstation.dou.modules.article.model.ArticlePreview;
import com.glitchstation.dou.modules.article.model.FullArticle;
import com.glitchstation.dou.modules.common.BasePresenter;

public interface FullArticleContract {

    interface View {
        void onArticleLoaded(FullArticle article);
        void onArticleLoadError();
        void onArticleBookmarked();
        void onArticleUnbookmarked();
        void onAddBookmarkError();
        void onRemoveBookmarkError();
    }

    abstract class Presenter extends BasePresenter {
        public abstract void getFullArticle(ArticlePreview preview, boolean cached);
        public abstract void addArticleToBookmarks(FullArticle article);
        public abstract void removeArticleFromBookmarks(FullArticle article);
    }
}
