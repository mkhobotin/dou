package com.glitchstation.dou.modules.category.presenter;

import com.glitchstation.dou.di.annotations.ActivityScope;
import com.glitchstation.dou.modules.article.config.ArticlePreviewConfig;
import com.glitchstation.dou.modules.article.model.ArticlePreview;
import com.glitchstation.dou.modules.category.contract.CategoryContract;
import com.glitchstation.dou.modules.eventbus.BaseEventService;
import com.glitchstation.dou.modules.eventbus.EventBus;

import java.util.ArrayList;

import javax.inject.Inject;

@ActivityScope
public class CategoryPresenter extends CategoryContract.Presenter {
    private CategoryContract.View view;
    private EventBus eventBus;
    private boolean isLoadingArticles = false;
    private boolean hasLoadedAllArticles = false;
    private int articlesOffset = 0;
    private static final int limit = 20;
    private String category;

    @Inject
    public CategoryPresenter(CategoryContract.View view, EventBus eventBus) {
        this.view = view;
        this.eventBus = eventBus;
    }

    @Override
    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public void getArticlesOfCategory() {
        if (!isLoadingArticles) {
            articlesOffset = 0;
            getArticlesOfCategory(articlesOffset, false);
        }
    }

    private void getArticlesOfCategory(int offset, boolean cached) {
        isLoadingArticles = true;
        ArticlePreviewConfig config = new ArticlePreviewConfig(category, offset, limit, cached);
        addSubscription(config, new SubscriptionListener<ArrayList<ArticlePreview>>() {
            @Override
            public void onSuccess(ArrayList<ArticlePreview> articlePreviews) {
                isLoadingArticles = false;
                if (articlePreviews.size() < limit) {
                    hasLoadedAllArticles = true;
                }
                boolean refresh = articlesOffset == 0;
                view.showCategoryArticles(articlePreviews, refresh);
                articlesOffset += limit;
            }

            @Override
            public void onError(Exception e) {
                isLoadingArticles = false;
                view.onArticlesLoadError();
            }
        });

    }

    @Override
    public BaseEventService getEventService() {
        return eventBus.category();
    }

    @Override
    public void onLoadMore() {
        // Try to get cached articles on first launch of view.
        // Although offset is 0 during updating (pull-refresh), pagination is not triggered in this case,
        // rather view calls presenter method
        boolean cached = articlesOffset == 0;
        getArticlesOfCategory(articlesOffset, cached);
    }

    @Override
    public boolean isLoading() {
        return isLoadingArticles;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return hasLoadedAllArticles;
    }
}
