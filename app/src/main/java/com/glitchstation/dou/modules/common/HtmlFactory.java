package com.glitchstation.dou.modules.common;

import android.support.annotation.Nullable;

import com.glitchstation.dou.modules.article.model.ArticleHtml;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

public class HtmlFactory extends Converter.Factory {

    @Inject
    public HtmlFactory() {
    }

    @Nullable
    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        if (type == ArticleHtml.class) {
            return getArticleHtmlConverter();
        }
        return null;
    }

    private Converter<ResponseBody, ArticleHtml> getArticleHtmlConverter() {
        return value -> {
            Document document = Jsoup.parse(value.string());

            String authorImageUrl = document
                .select("img.g-avatar")
                .attr("srcset")
                .replace(" 1.1x", "");

            document.selectFirst("div.b-post-info").remove();
            document.selectFirst("div.likely").remove();
            document.selectFirst("div.b-articles.b-articles__also").remove();
            document.selectFirst("a[id=upPageLnkId]").remove();
            document.selectFirst("a[id=btnSubsribe]").remove();
            document.selectFirst("div.fixed-menu > span").remove();

            document.select("div.thread-comments").remove();
            document.select("div.ct-hidden").removeClass("ct-hidden");

            for (Element element : document.select("div.bottom-line")) {
                if (element.selectFirst(":root > a") == null) {
                    element.remove();
                } else {
                    element.selectFirst("div.mobile-btn-group").remove();
                }
            }

            document.selectFirst("div.l-content.m-content").attr("style", "padding-top: 64px;");

            String html = "<html lang=\"ru\" xmlns:og=\"http://ogp.me/ns#\" class=\"is_mobile\">\n<head>" +
                "<link type=\"text/css\" rel=\"stylesheet\" href=\"site_files/built.v1641.b2b055f.css\">" +
                "<link type=\"text/css\" rel=\"stylesheet\" href=\"site_files/built.v1641.9dc82db.css\">" +
                "<script type=\"text/javascript\" src=\"site_files/built.v1553.e4f97d3.js\"></script>" +
                "<script type=\"text/javascript\" src=\"site_files/built.v1553.9bc68b4.js\"></script>" +
                "<link href=\"site_files/fotorama.css\" rel=\"stylesheet\">" +
                "<script src=\"site_files/fotorama.js\"></script>" +
                "</head><body class><div class=\"g-page\">" +
                document.selectFirst("div.l-content.m-content") +
                "</div></body></html>";

            float wpm = 220.0f;
            int readTime = Math.round(document.select("article").html().split("\\s+|&nbsp;").length / wpm);

            return new ArticleHtml(html, authorImageUrl, readTime);
        };
    }
}
