package com.glitchstation.dou.modules.store;

import com.glitchstation.dou.modules.article.store.ArticleStore;
import com.glitchstation.dou.modules.bookmark.store.BookmarkStore;
import com.glitchstation.dou.modules.category.store.CategoryStore;
import com.glitchstation.dou.modules.feed.store.FeedStore;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class StoreService {

    @Inject
    public StoreService(FeedStore feed, CategoryStore category, ArticleStore article, BookmarkStore bookmark) {
    }
}
