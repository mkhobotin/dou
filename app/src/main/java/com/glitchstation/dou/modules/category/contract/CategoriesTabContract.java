package com.glitchstation.dou.modules.category.contract;

import com.glitchstation.dou.modules.article.model.ArticlePreview;
import com.glitchstation.dou.modules.common.BasePresenter;

import java.util.ArrayList;

public interface CategoriesTabContract {

    interface View {

        void setCategoryArticles(String category, ArrayList<ArticlePreview> articles);

        void showCategories();

        void onCategoriesLoadError();
    }

    abstract class Presenter extends BasePresenter {

        public abstract void getArticlesOfCategories(String[] categories);
    }
}
