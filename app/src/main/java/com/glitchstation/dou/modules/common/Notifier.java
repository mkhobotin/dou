package com.glitchstation.dou.modules.common;

import android.content.Context;
import android.support.annotation.StringRes;
import android.widget.Toast;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class Notifier {

    @Inject
    public Notifier() {

    }

    public void showMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public void showMessage(Context context, @StringRes int resId) {
        showMessage(context, context.getResources().getString(resId));
    }
}
