package com.glitchstation.dou.modules.feed.store;

import android.annotation.SuppressLint;

import com.glitchstation.dou.modules.article.config.ArticlePreviewConfig;
import com.glitchstation.dou.modules.article.model.ArticlePreview;
import com.glitchstation.dou.modules.article.service.ArticleClient;
import com.glitchstation.dou.modules.common.ResponseModel;
import com.glitchstation.dou.modules.eventbus.EventBus;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

import static com.glitchstation.dou.modules.eventbus.EventType.DATA_RECEIVED;
import static com.glitchstation.dou.modules.eventbus.EventType.GET_DATA;

@Singleton
@SuppressLint("CheckResult")
public class FeedStore {
    private EventBus eventBus;
    private ArticleClient httpClient;

    @Inject
    public FeedStore(EventBus eventBus, ArticleClient httpClient) {
        this.eventBus = eventBus;
        this.httpClient = httpClient;
        this.eventBus.feed().on(GET_DATA).subscribe(o -> {
            if (o instanceof ArticlePreviewConfig) {
                getData((ArticlePreviewConfig) o);
            }
        });
    }

    private void getData(ArticlePreviewConfig config) {
        this.httpClient.getFeedArticles(config.getOffset())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(response -> {
                if (response.isSuccessful()) {
                    ArrayList<ArticlePreview> articlePreviews = response.body();
                    eventBus.feed().emit(DATA_RECEIVED, new ResponseModel<>(
                        config, articlePreviews));
                } else {
                    ResponseBody errorBody = response.errorBody();
                    if (errorBody != null) {
                        String errorMessage = errorBody.string();
                        eventBus.feed().emit(DATA_RECEIVED, new ResponseModel<>(
                            config, new Exception(errorMessage)));
                    }
                }
            }, throwable ->
                eventBus.feed().emit(DATA_RECEIVED, new ResponseModel<>(config, new Exception(throwable)))
            );
    }
}
