package com.glitchstation.dou.modules.category.store;

import android.annotation.SuppressLint;
import android.os.Handler;

import com.glitchstation.dou.modules.article.config.ArticlePreviewConfig;
import com.glitchstation.dou.modules.article.model.ArticlePreview;
import com.glitchstation.dou.modules.article.service.ArticleClient;
import com.glitchstation.dou.modules.common.ResponseModel;
import com.glitchstation.dou.modules.eventbus.EventBus;
import com.glitchstation.dou.modules.store.cache.BaseCacheService;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

import static com.glitchstation.dou.modules.eventbus.EventType.DATA_RECEIVED;
import static com.glitchstation.dou.modules.eventbus.EventType.GET_DATA;

@Singleton
@SuppressLint("CheckResult")
public class CategoryStore {
    private EventBus eventBus;
    private ArticleClient httpClient;
    private BaseCacheService cacheService;
    private long lastRequestTime;
    private final int delay = 300;

    @Inject
    public CategoryStore(EventBus eventBus, ArticleClient httpClient, BaseCacheService cacheService) {
        this.eventBus = eventBus;
        this.httpClient = httpClient;
        this.cacheService = cacheService;
        this.eventBus.category().on(GET_DATA).subscribe(o -> {
            if (o instanceof ArticlePreviewConfig) {
                getCategoryArticles((ArticlePreviewConfig) o);
            }
        });
    }

    private void getCategoryArticles(ArticlePreviewConfig config) {
        long now = System.currentTimeMillis();
        if (now - lastRequestTime < delay) {
            new Handler().postDelayed(() -> sendCategoryArticlesRequest(config),
                lastRequestTime + delay - now);
            lastRequestTime += delay;
        } else {
            new Handler().postDelayed(() -> sendCategoryArticlesRequest(config), delay);
            lastRequestTime = now + delay;
        }
    }

    private void sendCategoryArticlesRequest(ArticlePreviewConfig config) {
        if (config.isCached()) {
            ArrayList<ArticlePreview> articlePreviews = (ArrayList<ArticlePreview>) cacheService.getData(config);
            if (articlePreviews != null) {
                eventBus.category().emit(DATA_RECEIVED, new ResponseModel<>(config, articlePreviews));
                return;
            }
        }

        this.httpClient.getCategoryArticles(config.getCategory(), config.getLimit(), config.getOffset())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(response -> {
                if (response.isSuccessful()) {
                    ArrayList<ArticlePreview> articlePreviews = response.body();
                    cacheService.saveData(config, articlePreviews);
                    eventBus.category().emit(DATA_RECEIVED, new ResponseModel<>(
                        config, articlePreviews));
                } else {
                    ResponseBody errorBody = response.errorBody();
                    if (errorBody != null) {
                        String errorMessage = errorBody.string();
                        eventBus.category().emit(DATA_RECEIVED, new ResponseModel<>(
                            config, new Exception(errorMessage)));
                    }
                }
            }, throwable ->
                eventBus.category().emit(DATA_RECEIVED, new ResponseModel<>(config, new Exception(throwable)))
            );
    }
}
