package com.glitchstation.dou.modules.category.contract;

import com.glitchstation.dou.modules.article.model.ArticlePreview;
import com.glitchstation.dou.modules.common.BasePresenter;
import com.glitchstation.dou.modules.utils.pagination.Paginate;

import java.util.ArrayList;

public interface CategoryContract {

    interface View {

        void showCategoryArticles(ArrayList<ArticlePreview> articles, boolean refresh);

        void onArticlesLoadError();
    }

    abstract class Presenter extends BasePresenter implements Paginate.Callbacks {

        public abstract void setCategory(String category);

        public abstract void getArticlesOfCategory();
    }
}
