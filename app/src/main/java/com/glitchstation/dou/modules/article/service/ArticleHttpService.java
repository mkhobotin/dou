package com.glitchstation.dou.modules.article.service;

import com.glitchstation.dou.BuildConfig;
import com.glitchstation.dou.modules.common.GsonFactory;
import com.glitchstation.dou.modules.common.HtmlFactory;
import com.glitchstation.dou.modules.common.HttpService;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Singleton
public class ArticleHttpService implements HttpService {
    private GsonFactory gsonFactory;
    private HtmlFactory htmlFactory;
    private ArticleClient httpClient;
    private String userAgent = "Mozilla/5.0 (Linux; Android 8.0; Pixel 2 Build/OPD3.170816.012) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Mobile Safari/537.36";

    @Inject
    public ArticleHttpService(GsonFactory gsonFactory, HtmlFactory htmlFactory) {
        this.gsonFactory = gsonFactory;
        this.htmlFactory = htmlFactory;
    }

    private void initClient() {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClientBuilder.addInterceptor(loggingInterceptor);
        }

        Interceptor userAgentInterceptor = chain -> {
            Request userAgentRequest = chain.request()
                .newBuilder()
                .header("User-Agent", userAgent)
                .build();
            return chain.proceed(userAgentRequest);
        };

        httpClientBuilder.addInterceptor(userAgentInterceptor);
        OkHttpClient httpClient = httpClientBuilder.build();

        Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(API_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(htmlFactory)
            .addConverterFactory(GsonConverterFactory.create(gsonFactory.getGson()))
            .client(httpClient)
            .build();
        this.httpClient = retrofit.create(ArticleClient.class);
    }

    public ArticleClient getClient() {
        if (this.httpClient == null) {
            initClient();
        }
        return this.httpClient;
    }
}
