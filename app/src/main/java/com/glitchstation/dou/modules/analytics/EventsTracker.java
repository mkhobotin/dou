package com.glitchstation.dou.modules.analytics;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.ShareEvent;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class EventsTracker {
    private Answers answers;

    @Inject
    public EventsTracker() {
        answers = Answers.getInstance();
    }

    public void logContentView(String contentType) {
        answers.logContentView(new ContentViewEvent()
            .putContentType(contentType));
    }

    public void logShare(String shareMethod) {
        answers.logShare(new ShareEvent()
            .putMethod(shareMethod));
    }

    public void logCustomEvent(String message) {
        answers.logCustom(new CustomEvent(message));
    }

    public void logCustomEvent(String message, String attributeKey, String attributeValue) {
        answers.logCustom(new CustomEvent(message)
            .putCustomAttribute(attributeKey, attributeValue));
    }
}
