package com.glitchstation.dou.modules.article.store;

import android.annotation.SuppressLint;

import com.glitchstation.dou.modules.article.config.FullArticleConfig;
import com.glitchstation.dou.modules.article.model.FullArticle;
import com.glitchstation.dou.modules.article.service.ArticleClient;
import com.glitchstation.dou.modules.common.ResponseModel;
import com.glitchstation.dou.modules.eventbus.EventBus;
import com.glitchstation.dou.modules.store.cache.BaseCacheService;
import com.glitchstation.dou.modules.store.db.ArticleDBProvider;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

import static com.glitchstation.dou.modules.eventbus.EventType.DATA_RECEIVED;
import static com.glitchstation.dou.modules.eventbus.EventType.GET_DATA;

@Singleton
@SuppressLint("CheckResult")
public class ArticleStore {
    private EventBus eventBus;
    private ArticleClient httpClient;
    private BaseCacheService cacheService;
    private ArticleDBProvider dbProvider;

    @Inject
    public ArticleStore(EventBus eventBus, ArticleClient httpClient,
                        BaseCacheService cacheService, ArticleDBProvider dbProvider) {
        this.eventBus = eventBus;
        this.httpClient = httpClient;
        this.cacheService = cacheService;
        this.dbProvider = dbProvider;
        this.eventBus.article().on(GET_DATA).subscribe(o -> {
            if (o instanceof FullArticleConfig) {
                getFullArticle((FullArticleConfig) o);
            }
        });
    }

    private void getFullArticle(FullArticleConfig config) {
        if (config.isCached()) {
            FullArticle cacheArticle = (FullArticle) cacheService.getData(config);
            if (cacheArticle != null) {
                cacheArticle.setBookmarked(dbProvider.isBookmarked(cacheArticle));
                eventBus.article().emit(DATA_RECEIVED, new ResponseModel<>(config, cacheArticle));
                return;
            }
            try {
                FullArticle dbArticle = dbProvider.getFullArticle(config.getPreview());
                if (dbArticle != null) {
                    eventBus.article().emit(DATA_RECEIVED, new ResponseModel<>(config, dbArticle));
                    return;
                }
            } catch (Exception ignored) {}
        }

        this.httpClient.getArticleHtml(config.getPreview().getUrl())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(response -> {
                if (response.isSuccessful()) {
                    FullArticle fullArticle = new FullArticle(config.getPreview(), response.body());
                    fullArticle.setBookmarked(dbProvider.isBookmarked(fullArticle));
                    cacheService.saveData(config, fullArticle);
                    eventBus.article().emit(DATA_RECEIVED, new ResponseModel<>(
                        config, fullArticle));
                } else {
                    ResponseBody errorBody = response.errorBody();
                    if (errorBody != null) {
                        String errorMessage = errorBody.string();
                        eventBus.article().emit(DATA_RECEIVED, new ResponseModel<>(
                            config, new Exception(errorMessage)));
                    }
                }
            }, throwable ->
                eventBus.article().emit(DATA_RECEIVED, new ResponseModel<>(config, new Exception(throwable)))
            );
    }
}
