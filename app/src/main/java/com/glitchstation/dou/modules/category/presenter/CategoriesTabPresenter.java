package com.glitchstation.dou.modules.category.presenter;

import com.glitchstation.dou.di.annotations.FragmentScope;
import com.glitchstation.dou.modules.article.config.ArticlePreviewConfig;
import com.glitchstation.dou.modules.article.model.ArticlePreview;
import com.glitchstation.dou.modules.category.contract.CategoriesTabContract;
import com.glitchstation.dou.modules.eventbus.BaseEventService;
import com.glitchstation.dou.modules.eventbus.EventBus;

import java.util.ArrayList;

import javax.inject.Inject;

@FragmentScope
public class CategoriesTabPresenter extends CategoriesTabContract.Presenter {
    private CategoriesTabContract.View view;
    private EventBus eventBus;
    private int categoriesCount;
    private int responseCount;
    private boolean error;

    @Inject
    public CategoriesTabPresenter(CategoriesTabContract.View view, EventBus eventBus) {
        this.view = view;
        this.eventBus = eventBus;
    }

    @Override
    public void getArticlesOfCategories(String[] categories) {
        responseCount = 0;
        error = false;
        categoriesCount = categories.length;
        for (String category: categories) {
            ArticlePreviewConfig config = new ArticlePreviewConfig(category, 0, 5, false);
            addSubscription(config, new SubscriptionListener<ArrayList<ArticlePreview>>() {
                @Override
                public void onSuccess(ArrayList<ArticlePreview> articles) {
                    responseCount++;
                    view.setCategoryArticles(category, articles);
                    if (responseCount == categoriesCount) {
                        if (!error) {
                            view.showCategories();
                        } else {
                            view.onCategoriesLoadError();
                        }
                    }
                }

                @Override
                public void onError(Exception e) {
                    responseCount++;
                    error = true;
                    if (responseCount == categoriesCount) {
                        view.onCategoriesLoadError();
                    }
                }
            });
        }
    }

    @Override
    public BaseEventService getEventService() {
        return eventBus.category();
    }
}
