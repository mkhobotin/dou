package com.glitchstation.dou.modules.article.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;

public class ArticlePreview extends RealmObject implements Serializable {
    @SerializedName("id")
    private long id;
    @SerializedName("title")
    private String title;
    @SerializedName("url")
    private String url;
    @SerializedName("category")
    private String category;
    @SerializedName("category_url")
    private String categoryUrl;
    @SerializedName("announcement")
    private String announcement;
    @SerializedName("tags")
    private String tags;
    @SerializedName("pageviews")
    private int pageViews;
    @SerializedName("comments_count")
    private int commentsCount;
    @SerializedName("img_big_2x")
    private String imageUrl;
    @SerializedName("author_name")
    private String authorName;
    @SerializedName("author_url")
    private String authorUrl;
    @SerializedName("published")
    private String published;

    public ArticlePreview() {
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getCategory() {
        return category;
    }

    public String getCategoryUrl() {
        return categoryUrl;
    }

    public String getAnnouncement() {
        return announcement;
    }

    public String getTags() {
        return tags;
    }

    public int getPageViews() {
        return pageViews;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getAuthorUrl() {
        return authorUrl;
    }

    public String getPublished() {
        return published;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArticlePreview that = (ArticlePreview) o;

        if (id != that.id) return false;
        if (pageViews != that.pageViews) return false;
        if (commentsCount != that.commentsCount) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (url != null ? !url.equals(that.url) : that.url != null) return false;
        if (category != null ? !category.equals(that.category) : that.category != null)
            return false;
        if (categoryUrl != null ? !categoryUrl.equals(that.categoryUrl) : that.categoryUrl != null)
            return false;
        if (announcement != null ? !announcement.equals(that.announcement) : that.announcement != null)
            return false;
        if (tags != null ? !tags.equals(that.tags) : that.tags != null) return false;
        if (imageUrl != null ? !imageUrl.equals(that.imageUrl) : that.imageUrl != null)
            return false;
        if (authorName != null ? !authorName.equals(that.authorName) : that.authorName != null)
            return false;
        if (authorUrl != null ? !authorUrl.equals(that.authorUrl) : that.authorUrl != null)
            return false;
        return published != null ? published.equals(that.published) : that.published == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (categoryUrl != null ? categoryUrl.hashCode() : 0);
        result = 31 * result + (announcement != null ? announcement.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        result = 31 * result + pageViews;
        result = 31 * result + commentsCount;
        result = 31 * result + (imageUrl != null ? imageUrl.hashCode() : 0);
        result = 31 * result + (authorName != null ? authorName.hashCode() : 0);
        result = 31 * result + (authorUrl != null ? authorUrl.hashCode() : 0);
        result = 31 * result + (published != null ? published.hashCode() : 0);
        return result;
    }
}
