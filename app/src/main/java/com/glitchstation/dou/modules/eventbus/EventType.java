package com.glitchstation.dou.modules.eventbus;

public enum EventType {
    GET_DATA, SAVE_DATA, DELETE_DATA, DATA_RECEIVED, DATA_SAVED, DATA_DELETED
}
