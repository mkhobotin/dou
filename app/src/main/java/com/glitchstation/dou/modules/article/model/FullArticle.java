package com.glitchstation.dou.modules.article.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class FullArticle extends RealmObject {
    @PrimaryKey
    private long id;
    private ArticlePreview preview;
    private ArticleHtml html;
    private boolean isBookmarked;

    public FullArticle() {
    }

    public FullArticle(ArticlePreview preview, ArticleHtml html) {
        this.id = preview.getId();
        this.preview = preview;
        this.html = html;
    }

    public long getId() {
        return id;
    }

    public ArticlePreview getPreview() {
        return preview;
    }

    public ArticleHtml getHtml() {
        return html;
    }

    public void setBookmarked(boolean bookmarked) {
        isBookmarked = bookmarked;
    }

    public boolean isBookmarked() {
        return isBookmarked;
    }
}
