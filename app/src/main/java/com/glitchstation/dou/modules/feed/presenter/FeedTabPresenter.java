package com.glitchstation.dou.modules.feed.presenter;

import com.glitchstation.dou.di.annotations.FragmentScope;
import com.glitchstation.dou.modules.article.config.ArticlePreviewConfig;
import com.glitchstation.dou.modules.article.model.ArticlePreview;
import com.glitchstation.dou.modules.eventbus.BaseEventService;
import com.glitchstation.dou.modules.eventbus.EventBus;
import com.glitchstation.dou.modules.feed.contract.FeedTabContract;

import java.util.ArrayList;

import javax.inject.Inject;

@FragmentScope
public class FeedTabPresenter extends FeedTabContract.Presenter {
    private FeedTabContract.View view;
    private EventBus eventBus;
    private boolean isLoadingArticle = false;
    private boolean hasLoadedAllArticles = false;
    private int articleOffset = 0;
    private static final int limit = 20;

    @Inject
    public FeedTabPresenter(FeedTabContract.View view, EventBus eventBus) {
        this.view = view;
        this.eventBus = eventBus;
    }

    @Override
    public void getFeedArticles() {
        if (!isLoadingArticle) {
            articleOffset = 0;
            getFeedArticles(articleOffset);
        }
    }

    private void getFeedArticles(int offset) {
        isLoadingArticle = true;
        ArticlePreviewConfig config = new ArticlePreviewConfig(null, offset, 20, false);
        addSubscription(config, new SubscriptionListener<ArrayList<ArticlePreview>>() {
            @Override
            public void onSuccess(ArrayList<ArticlePreview> articlePreviews) {
                isLoadingArticle = false;
                if (articlePreviews.size() < limit) {
                    hasLoadedAllArticles = true;
                }
                boolean refresh = articleOffset == 0;
                view.showArticles(articlePreviews, refresh);
                articleOffset += limit;
            }

            @Override
            public void onError(Exception e) {
                isLoadingArticle = false;
                view.onArticlesLoadError();
            }
        });
    }

    @Override
    public BaseEventService getEventService() {
        return eventBus.feed();
    }

    @Override
    public void onLoadMore() {
        getFeedArticles(articleOffset);
    }

    @Override
    public boolean isLoading() {
        return isLoadingArticle;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return hasLoadedAllArticles;
    }
}
