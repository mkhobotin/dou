package com.glitchstation.dou.modules.category.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.glitchstation.dou.R;
import com.glitchstation.dou.di.annotations.ActivityScope;
import com.glitchstation.dou.modules.article.model.ArticlePreview;
import com.glitchstation.dou.modules.article.view.ArticlePreviewListAdapter;
import com.glitchstation.dou.modules.category.contract.CategoryContract;
import com.glitchstation.dou.modules.common.Notifier;
import com.glitchstation.dou.modules.utils.pagination.Paginate;
import com.glitchstation.dou.modules.utils.pagination.RecyclerPaginate;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindArray;
import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import pl.droidsonroids.gif.GifImageView;

@ActivityScope
public class CategoryActivity extends AppCompatActivity implements CategoryContract.View {
    @Inject
    CategoryContract.Presenter presenter;
    @Inject
    ArticlePreviewListAdapter adapter;
    @Inject
    Notifier notifier;
    @BindView(R.id.category_recycler_view)
    RecyclerView categoryRecyclerView;
    @BindView(R.id.category_error_img)
    GifImageView errorImage;
    @BindView(R.id.category_swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.category_progress_bar)
    ProgressBar progressBar;
    @BindArray(R.array.categories_titles)
    String[] categoryTitles;
    @BindArray(R.array.categories_values)
    String[] categoryValues;
    private Paginate paginate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        ButterKnife.bind(this);
        setUi();
    }

    @Override
    protected void onDestroy() {
        presenter.unsubscribe();
        super.onDestroy();
    }

    private void setUi() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            int categoryPosition = bundle.getInt("categoryPosition", -1);
            if (categoryPosition != -1) {
                presenter.setCategory(categoryValues[categoryPosition]);
                if (actionBar != null) {
                    actionBar.setTitle(categoryTitles[categoryPosition]);
                }
            }
        }

        categoryRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        categoryRecyclerView.setAdapter(adapter);
        paginate = RecyclerPaginate
            .with(categoryRecyclerView, presenter)
            .setLoadingTriggerThreshold(10)
            .build();
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.getArticlesOfCategory());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showCategoryArticles(ArrayList<ArticlePreview> articles, boolean refresh) {
        swipeRefreshLayout.setRefreshing(false);
        progressBar.setVisibility(View.GONE);
        errorImage.setVisibility(View.GONE);
        categoryRecyclerView.setVisibility(View.VISIBLE);
        if (refresh) {
            adapter.setArticles(articles);
        } else {
            adapter.addArticles(articles);
        }
    }

    @Override
    public void onArticlesLoadError() {
        progressBar.setVisibility(View.GONE);
        if (adapter.getItemCount() != 0) {
            if (swipeRefreshLayout.isRefreshing()) {
                notifier.showMessage(this, R.string.no_connection_error);
            } else {
                paginate.showErrorRow();
            }
        } else {
            errorImage.setVisibility(View.VISIBLE);
        }
        swipeRefreshLayout.setRefreshing(false);
    }
}
