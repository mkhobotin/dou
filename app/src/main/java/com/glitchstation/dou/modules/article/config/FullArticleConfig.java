package com.glitchstation.dou.modules.article.config;

import com.glitchstation.dou.modules.article.model.ArticlePreview;
import com.glitchstation.dou.modules.eventbus.EventConfig;

public class FullArticleConfig implements EventConfig {
    private ArticlePreview preview;
    private boolean cached;

    public FullArticleConfig(ArticlePreview preview, boolean cached) {
        this.preview = preview;
        this.cached = cached;
    }

    public ArticlePreview getPreview() {
        return preview;
    }

    public boolean isCached() {
        return cached;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FullArticleConfig that = (FullArticleConfig) o;

        return preview != null ? preview.equals(that.preview) : that.preview == null;
    }

    @Override
    public int hashCode() {
        return preview != null ? preview.hashCode() : 0;
    }
}
