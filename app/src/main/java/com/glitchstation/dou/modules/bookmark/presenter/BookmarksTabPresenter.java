package com.glitchstation.dou.modules.bookmark.presenter;

import com.glitchstation.dou.di.annotations.FragmentScope;
import com.glitchstation.dou.modules.article.model.FullArticle;
import com.glitchstation.dou.modules.bookmark.config.BookmarkConfig;
import com.glitchstation.dou.modules.bookmark.contract.BookmarksTabContract;
import com.glitchstation.dou.modules.eventbus.BaseEventService;
import com.glitchstation.dou.modules.eventbus.EventBus;

import java.util.ArrayList;

import javax.inject.Inject;

import static com.glitchstation.dou.modules.eventbus.EventType.GET_DATA;

@FragmentScope
public class BookmarksTabPresenter extends BookmarksTabContract.Presenter {
    private BookmarksTabContract.View view;
    private EventBus eventBus;

    @Inject
    public BookmarksTabPresenter(BookmarksTabContract.View view, EventBus eventBus) {
        this.view = view;
        this.eventBus = eventBus;
    }

    @Override
    public void getBookmarks() {
        BookmarkConfig config = new BookmarkConfig(null, GET_DATA);
        addSubscription(config, new SubscriptionListener<ArrayList<FullArticle>>() {
            @Override
            public void onSuccess(ArrayList<FullArticle> articles) {
                view.showBookmarks(articles);
            }

            @Override
            public void onError(Exception e) {
                view.onBookmarksLoadError();
            }
        });
    }

    @Override
    public BaseEventService getEventService() {
        return eventBus.bookmark();
    }
}
