package com.glitchstation.dou.modules.main.presenter;

import com.glitchstation.dou.di.annotations.ActivityScope;
import com.glitchstation.dou.modules.eventbus.BaseEventService;
import com.glitchstation.dou.modules.eventbus.EventBus;
import com.glitchstation.dou.modules.main.contract.MainContract;

import javax.inject.Inject;

@ActivityScope
public class MainPresenter extends MainContract.Presenter {
    private MainContract.View view;
    private EventBus eventBus;

    @Inject
    public MainPresenter(MainContract.View view, EventBus eventBus) {
        this.view = view;
        this.eventBus = eventBus;
    }

    @Override
    public BaseEventService getEventService() {
        return null;
    }
}
