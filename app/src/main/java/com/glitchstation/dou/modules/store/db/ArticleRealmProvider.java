package com.glitchstation.dou.modules.store.db;

import com.glitchstation.dou.modules.article.model.ArticlePreview;
import com.glitchstation.dou.modules.article.model.FullArticle;
import com.glitchstation.dou.modules.utils.ArticleComparator;

import java.util.ArrayList;
import java.util.Collections;

import javax.inject.Inject;

import io.realm.Realm;

public class ArticleRealmProvider implements ArticleDBProvider {
    private Realm realm;

    @Inject
    public ArticleRealmProvider(Realm realm) {
        this.realm = realm;
    }

    @Override
    public void saveArticle(FullArticle article) {
        realm.executeTransaction(realm -> realm.copyToRealm(article).setBookmarked(true));
    }

    @Override
    public void removeArticle(FullArticle article) {
        realm.executeTransaction(realm -> realm
            .where(FullArticle.class)
            .equalTo("id", article.getId())
            .findFirst()
            .deleteFromRealm());
    }

    @Override
    public FullArticle getFullArticle(ArticlePreview articlePreview) {
        FullArticle fullArticle = realm
            .where(FullArticle.class)
            .equalTo("id", articlePreview.getId())
            .findFirst();
        if (fullArticle == null) {
            return null;
        } else {
            return realm.copyFromRealm(fullArticle);
        }
    }

    @Override
    public ArrayList<FullArticle> getAllArticles() {
        ArrayList<FullArticle> articles = new ArrayList<>(
            realm.copyFromRealm(realm.where(FullArticle.class).findAll()));
        Collections.sort(articles, new ArticleComparator());
        return articles;
    }

    @Override
    public boolean isBookmarked(FullArticle article) {
        return realm
            .where(FullArticle.class)
            .equalTo("id", article.getId())
            .findFirst() != null;
    }
}
