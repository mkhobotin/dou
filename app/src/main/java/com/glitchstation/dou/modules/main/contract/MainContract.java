package com.glitchstation.dou.modules.main.contract;

import com.glitchstation.dou.modules.common.BasePresenter;

public interface MainContract {

    interface View {
    }

    abstract class Presenter extends BasePresenter {
    }
}
