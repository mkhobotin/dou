package com.glitchstation.dou.modules.article.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.glitchstation.dou.R;

public class PreviewImageView extends AppCompatImageView {
    private static final float imageAspectRatio = 0.548f;
    private float radius;
    private RectF rect;
    private Path clipPath;
    private Paint overlayPaint;

    public PreviewImageView(Context context) {
        super(context);
        init(context);
    }

    public PreviewImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public PreviewImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        radius = context.getResources().getDimension(R.dimen.preview_image_corner_radius);
        rect = new RectF();
        clipPath = new Path();
        overlayPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        overlayPaint.setColor(0x0D000000);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        clipPath.addRoundRect(rect, radius, radius, Path.Direction.CW);
        canvas.clipPath(clipPath);
        super.onDraw(canvas);
        canvas.drawRect(rect, overlayPaint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = Math.round(width * imageAspectRatio);
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        rect.set(0, 0, w, h);
    }
}
