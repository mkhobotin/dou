package com.glitchstation.dou.modules.statics;

import android.app.Activity;
import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.glitchstation.dou.di.components.DaggerApplicationComponent;
import com.glitchstation.dou.modules.eventbus.EventBus;
import com.glitchstation.dou.modules.store.StoreService;

import io.fabric.sdk.android.Fabric;
import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import io.realm.Realm;

@Singleton
public class MyApplication extends Application implements HasActivityInjector {
    @Inject
    DispatchingAndroidInjector<Activity> dispatchingActivityInjector;
    @Inject
    EventBus eventBus;
    @Inject
    StoreService storeService;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Answers(), new Crashlytics());
        Realm.init(this);
        DaggerApplicationComponent
            .builder()
            .application(this)
            .build()
            .inject(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingActivityInjector;
    }
}
