package com.glitchstation.dou.modules.store.db;

import com.glitchstation.dou.modules.article.model.ArticlePreview;
import com.glitchstation.dou.modules.article.model.FullArticle;

import java.util.ArrayList;

public interface ArticleDBProvider {
    void saveArticle(FullArticle article);
    void removeArticle(FullArticle article);
    FullArticle getFullArticle(ArticlePreview articlePreview);
    ArrayList<FullArticle> getAllArticles();
    boolean isBookmarked(FullArticle article);
}
