package com.glitchstation.dou.modules.common;

import com.glitchstation.dou.modules.eventbus.BaseEventService;
import com.glitchstation.dou.modules.eventbus.EventConfig;
import com.glitchstation.dou.modules.eventbus.EventType;

import java.util.HashMap;

import io.reactivex.disposables.Disposable;

import static com.glitchstation.dou.modules.eventbus.EventType.DATA_RECEIVED;
import static com.glitchstation.dou.modules.eventbus.EventType.GET_DATA;

public abstract class BasePresenter {
    private HashMap<EventConfig, Disposable> disposables;

    public BasePresenter() {
        disposables = new HashMap<>();
    }

    public void unsubscribe() {
        for (Disposable disposable : disposables.values()) {
            disposable.dispose();
        }
        disposables.clear();
    }

    public <T> void addSubscription(EventConfig config, SubscriptionListener<T> listener) {
        addSubscription(config, getEventService(), listener);
    }

    public <T> void addSubscription(EventConfig config, BaseEventService eventService,
                                    SubscriptionListener<T> listener) {
        addSubscription(config, eventService, GET_DATA, DATA_RECEIVED, listener);
    }

    public <T> void addSubscription(EventConfig config, BaseEventService eventService,
            EventType emitEventType, EventType receiveEventType, SubscriptionListener<T> listener) {
        if (!disposables.containsKey(config)) {
            Disposable disposable = eventService.on(receiveEventType).subscribe(o -> {
                if (o instanceof ResponseModel) {
                    ResponseModel<T> response = (ResponseModel<T>) o;
                    if (response.getConfig().equals(config)) {
                        if (listener != null) {
                            if (response.isSuccessful()) {
                                listener.onSuccess(response.getData());
                            } else {
                                listener.onError(response.getException());
                            }
                        }
                    }
                }
            });
            disposables.put(config, disposable);
        }
        eventService.emit(emitEventType, config);
    }

    public abstract BaseEventService getEventService();

    public interface SubscriptionListener<T> {
        void onSuccess(T data);

        void onError(Exception e);
    }
}
