package com.glitchstation.dou.modules.article.model;

import io.realm.RealmObject;

public class ArticleHtml extends RealmObject {
    private String htmlContent;
    private String authorImageUrl;
    private int readTime;

    public ArticleHtml() {
    }

    public ArticleHtml(String htmlContent, String authorImageUrl, int readTime) {
        this.htmlContent = htmlContent;
        this.authorImageUrl = authorImageUrl;
        this.readTime = readTime;
    }

    public String getHtmlContent() {
        return htmlContent;
    }

    public String getAuthorImageUrl() {
        return authorImageUrl;
    }

    public int getReadTime() {
        return readTime;
    }
}
