package com.glitchstation.dou.modules.eventbus;

import io.reactivex.Observable;

public interface EventService {

   <T> Observable<T> on(EventType eventType);

   <T> void emit(EventType eventType, T data);
}
