package com.glitchstation.dou.modules.eventbus;

import java.util.HashMap;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class EventBus {
    private final HashMap<String, BaseEventService> register;

    @Inject
    public EventBus() {
        register = new HashMap<>();
    }

    public BaseEventService feed() {
        return getEventService("feed");
    }

    public BaseEventService category() {
        return getEventService("category");
    }

    public BaseEventService article() {
        return getEventService("article");
    }

    public BaseEventService bookmark() {
        return getEventService("bookmark");
    }

    private BaseEventService getEventService(String name) {
        if (!register.containsKey(name)) {
            register.put(name, new BaseEventService());
        }
        return register.get(name);
    }
}