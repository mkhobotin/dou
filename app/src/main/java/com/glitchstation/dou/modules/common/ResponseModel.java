package com.glitchstation.dou.modules.common;

import com.glitchstation.dou.modules.eventbus.EventConfig;

public class ResponseModel<T> {
    private EventConfig config;
    private T data;
    private Exception exception;

    private ResponseModel() {}

    public ResponseModel(EventConfig config) {
        this.config = config;
    }

    public ResponseModel(EventConfig config, T data) {
        this.config = config;
        this.data = data;
    }

    public ResponseModel(EventConfig config, Exception exception) {
        this.config = config;
        this.exception = exception;
    }

    public EventConfig getConfig() {
        return config;
    }

    public T getData() {
        return data;
    }

    public Exception getException() {
        return exception;
    }

    public boolean isSuccessful() {
        return exception == null;
    }
}
