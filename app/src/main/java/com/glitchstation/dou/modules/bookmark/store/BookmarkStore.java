package com.glitchstation.dou.modules.bookmark.store;

import android.annotation.SuppressLint;

import com.glitchstation.dou.modules.bookmark.config.BookmarkConfig;
import com.glitchstation.dou.modules.common.ResponseModel;
import com.glitchstation.dou.modules.eventbus.EventBus;
import com.glitchstation.dou.modules.store.db.ArticleDBProvider;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.glitchstation.dou.modules.eventbus.EventType.DATA_DELETED;
import static com.glitchstation.dou.modules.eventbus.EventType.DATA_RECEIVED;
import static com.glitchstation.dou.modules.eventbus.EventType.DATA_SAVED;
import static com.glitchstation.dou.modules.eventbus.EventType.DELETE_DATA;
import static com.glitchstation.dou.modules.eventbus.EventType.GET_DATA;
import static com.glitchstation.dou.modules.eventbus.EventType.SAVE_DATA;

@Singleton
@SuppressLint("CheckResult")
public class BookmarkStore {
    private EventBus eventBus;
    private ArticleDBProvider dbProvider;

    @Inject
    public BookmarkStore(EventBus eventBus, ArticleDBProvider dbProvider) {
        this.eventBus = eventBus;
        this.dbProvider = dbProvider;
        this.eventBus.bookmark().on(SAVE_DATA).subscribe(o -> {
            if (o instanceof BookmarkConfig) {
                saveArticle((BookmarkConfig) o);
            }
        });
        this.eventBus.bookmark().on(GET_DATA).subscribe(o -> {
            if (o instanceof BookmarkConfig) {
                getAllArticles((BookmarkConfig) o);
            }
        });
        this.eventBus.bookmark().on(DELETE_DATA).subscribe(o -> {
            if (o instanceof BookmarkConfig) {
                removeArticle((BookmarkConfig) o);
            }
        });
    }

    private void saveArticle(BookmarkConfig config) {
        try {
            dbProvider.saveArticle(config.getArticle());
            eventBus.bookmark().emit(DATA_SAVED, new ResponseModel<>(config));
        } catch (Exception e) {
            eventBus.bookmark().emit(DATA_SAVED, new ResponseModel<>(config, e));
        }
    }

    private void getAllArticles(BookmarkConfig config) {
        // TODO: get only article previews from db
        try {
            eventBus.bookmark().emit(DATA_RECEIVED, new ResponseModel<>(config, dbProvider.getAllArticles()));
        } catch (Exception e) {
            eventBus.bookmark().emit(DATA_RECEIVED, new ResponseModel<>(config, e));
        }
    }

    private void removeArticle(BookmarkConfig config) {
        try {
            dbProvider.removeArticle(config.getArticle());
            eventBus.bookmark().emit(DATA_DELETED, new ResponseModel<>(config));
        } catch (Exception e) {
            eventBus.bookmark().emit(DATA_DELETED, new ResponseModel<>(config, e));
        }
    }
}
