package com.glitchstation.dou.modules.eventbus;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class BaseEventService implements EventService {
    protected HashMap<EventType, PublishSubject> register;

    @Inject
    public BaseEventService() {
        this.register = new HashMap<>();
    }

    public <T> Observable<T> on(EventType eventType) {
        return this.getEntry(eventType);
    }

    public <T> void emit(EventType eventType, T data) {
        this.<T>getEntry(eventType).onNext(data);
    }

    protected <T> PublishSubject<T> getEntry(EventType eventType) {
        if (!this.register.containsKey(eventType)) {
            this.<T>createRegisterEntry(eventType);
        }
        return this.register.get(eventType);
    }

    protected <T> void createRegisterEntry(EventType eventType) {
        this.register.put(eventType, PublishSubject.create());
    }
}
