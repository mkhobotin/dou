package com.glitchstation.dou.modules.article.view;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.glitchstation.dou.R;
import com.glitchstation.dou.di.annotations.ActivityContext;
import com.glitchstation.dou.modules.analytics.EventsTracker;
import com.glitchstation.dou.modules.article.model.ArticlePreview;
import com.glitchstation.dou.modules.utils.DateFormatter;

import java.text.ParseException;
import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArticlePreviewListAdapter
        extends RecyclerView.Adapter<ArticlePreviewListAdapter.ArticlePreviewViewHolder> {
    public static final float narrowedSizeMultiplier = 0.85f;
    private ArrayList<ArticlePreview> articlePreviews;
    private Context context;
    private ListType type;
    private DateFormatter dateFormatter;
    private EventsTracker eventsTracker;

    @Inject
    public ArticlePreviewListAdapter(@ActivityContext Context context, EventsTracker eventsTracker) {
        this(context, ListType.VERTICAL, eventsTracker);
    }

    public ArticlePreviewListAdapter(Context context, ListType type, EventsTracker eventsTracker) {
        this.context = context;
        this.type = type;
        this.eventsTracker = eventsTracker;
        articlePreviews = new ArrayList<>();
        dateFormatter = new DateFormatter();
    }

    @NonNull
    @Override
    public ArticlePreviewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_article_preview, parent, false);
        if (type == ListType.HORIZONTAL) {
            ViewGroup.LayoutParams viewLayoutParams = view.getLayoutParams();
            viewLayoutParams.width = Math.round(narrowedSizeMultiplier * Resources.getSystem().getDisplayMetrics().widthPixels);
            view.setLayoutParams(viewLayoutParams);
        }
        return new ArticlePreviewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticlePreviewViewHolder holder, int position) {
        ArticlePreview article = articlePreviews.get(position);
        if (type == ListType.HORIZONTAL) {
            holder.title.setMaxLines(2);
        } else {
            holder.title.setMaxLines(Integer.MAX_VALUE);
        }
        holder.title.setText(article.getTitle());
        try {
            holder.published.setText(dateFormatter.formatDate(article.getPublished()));
        } catch (ParseException e) {
            holder.published.setText(article.getPublished());
        }
        holder.category.setText(article.getCategory());
        holder.pageViews.setText(String.valueOf(article.getPageViews()));
        if (type == ListType.VERTICAL) {
            holder.commentsCount.setVisibility(View.VISIBLE);
            holder.commentsCount.setText(String.valueOf(article.getCommentsCount()));
        } else {
            holder.commentsCount.setVisibility(View.GONE);
        }
        Glide.with(context).load(article.getImageUrl()).into(holder.image);
        if (type == ListType.HORIZONTAL || position == getItemCount() - 1) {
            holder.divider.setVisibility(View.GONE);
        } else {
            holder.divider.setVisibility(View.VISIBLE);
        }
        holder.itemView.setOnClickListener(v -> {
            eventsTracker.logContentView("Article");
            Intent intent = new Intent(context, FullArticleActivity.class);
            intent.putExtra("articlePreview", article);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return articlePreviews.size();
    }

    public void setArticles(ArrayList<ArticlePreview> articlePreviews) {
        this.articlePreviews = articlePreviews;
        notifyDataSetChanged();
    }

    public void addArticles(ArrayList<ArticlePreview> articlePreviews) {
        this.articlePreviews.addAll(articlePreviews);
        notifyDataSetChanged();
    }

    class ArticlePreviewViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.article_preview_title)
        TextView title;
        @BindView(R.id.article_preview_published)
        TextView published;
        @BindView(R.id.article_preview_category)
        TextView category;
        @BindView(R.id.article_preview_page_views)
        TextView pageViews;
        @BindView(R.id.article_preview_comments_count)
        TextView commentsCount;
        @BindView(R.id.article_preview_image)
        ImageView image;
        @BindView(R.id.article_preview_divider)
        View divider;

        ArticlePreviewViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public enum ListType {
        VERTICAL, HORIZONTAL
    }
}
