package com.glitchstation.dou.modules.article.view;

import android.content.res.Resources;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

public class WebViewGestureDetector extends GestureDetector.SimpleOnGestureListener {
    private Toolbar toolbar;
    private int hideOffset;
    private int showOffset;

    public WebViewGestureDetector(Toolbar toolbar) {
        this.toolbar = toolbar;
        hideOffset = Resources.getSystem().getDisplayMetrics().heightPixels / 100;
        showOffset = Resources.getSystem().getDisplayMetrics().heightPixels / 30;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if (e1 == null || e2 == null || e1.getPointerCount() > 1 || e2.getPointerCount() > 1) return false;

        if (e1.getY() - e2.getY() > hideOffset) {
            toolbar.animate().translationY(-toolbar.getHeight()).setInterpolator(new AccelerateInterpolator());
        } else if (e2.getY() - e1.getY() > showOffset) {
            toolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator());
        }
        return false;
    }
}
