package com.glitchstation.dou.modules.main.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.glitchstation.dou.R;
import com.glitchstation.dou.di.annotations.ActivityScope;
import com.glitchstation.dou.modules.analytics.EventsTracker;
import com.glitchstation.dou.modules.main.contract.MainContract;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

@ActivityScope
public class MainActivity extends AppCompatActivity
    implements MainContract.View, HasSupportFragmentInjector {
    @Inject
    MainContract.Presenter presenter;
    @Inject
    MainBottomNavAdapter adapter;
    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingFragmentInjector;
    @Inject
    EventsTracker eventsTracker;
    @BindView(R.id.main_view_pager)
    ViewPager viewPager;
    @BindView(R.id.main_bottom_nav)
    BottomNavigationViewEx bottomNav;
    private ActionBar actionBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setUi();
    }

    @Override
    protected void onDestroy() {
        presenter.unsubscribe();
        super.onDestroy();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingFragmentInjector;
    }

    private void setUi() {
        actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.feed);

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(adapter.getCount() - 1);

        bottomNav.enableShiftingMode(false);
        bottomNav.enableItemShiftingMode(false);
        bottomNav.setTextVisibility(false);
        bottomNav.setupWithViewPager(viewPager);
        bottomNav.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.bottom_nav_feed:
                    actionBar.setTitle(R.string.feed);
                    eventsTracker.logCustomEvent("Menu", "tab", "feed");
                    break;
                case R.id.bottom_nav_categories:
                    actionBar.setTitle(R.string.categories);
                    eventsTracker.logCustomEvent("Menu", "tab", "categories");
                    break;
                case R.id.bottom_nav_bookmarks:
                    actionBar.setTitle(R.string.bookmarks);
                    eventsTracker.logCustomEvent("Menu", "tab", "bookmarks");
                    break;
            }
            return true;
        });
        bottomNav.setOnNavigationItemReselectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.bottom_nav_feed:
                    eventsTracker.logCustomEvent("Menu reselect", "tab", "feed");
                    break;
                case R.id.bottom_nav_categories:
                    eventsTracker.logCustomEvent("Menu reselect", "tab", "categories");
                    break;
                case R.id.bottom_nav_bookmarks:
                    eventsTracker.logCustomEvent("Menu reselect", "tab", "bookmarks");
                    break;
            }
        });
    }
}
