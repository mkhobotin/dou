package com.glitchstation.dou.modules.splash.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.glitchstation.dou.R;
import com.glitchstation.dou.di.annotations.ActivityScope;
import com.glitchstation.dou.modules.main.view.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

@ActivityScope
public class SplashActivity extends AppCompatActivity {
    @BindView(R.id.splash_image_view)
    GifImageView splashImageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        setUi();
    }

    private void setUi() {
        GifDrawable gifDrawable = (GifDrawable) splashImageView.getDrawable();
        gifDrawable.addAnimationListener(loopNumber -> {
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        });
        gifDrawable.start();
    }
}
