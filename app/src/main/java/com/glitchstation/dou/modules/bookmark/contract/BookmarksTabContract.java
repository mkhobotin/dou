package com.glitchstation.dou.modules.bookmark.contract;

import com.glitchstation.dou.modules.article.model.FullArticle;
import com.glitchstation.dou.modules.common.BasePresenter;

import java.util.ArrayList;

public interface BookmarksTabContract {

    interface View {
        void showBookmarks(ArrayList<FullArticle> articles);
        void onBookmarksLoadError();
    }

    abstract class Presenter extends BasePresenter {
        public abstract void getBookmarks();
    }
}
